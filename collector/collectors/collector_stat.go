package collectors

import (
	"log"
	"time"

	"fmt"

	"gitlab.com/FluffGlitter/sysmon/bus"
	"gitlab.com/FluffGlitter/sysmon/collector"
	"gitlab.com/FluffGlitter/sysmon/common/proc/stat"
	"gitlab.com/FluffGlitter/sysmon/metric"
	"gitlab.com/FluffGlitter/sysmon/options"
)

// StatCollectorName - Name of the Stat Collector
const StatCollectorName = "Stat"
const statCollectorDesc = "Collector to capture the data from /proc/stat"

const (
	statUser = iota
	statNice
	statSystem
	statIdle
	statIO
	statIrq
	statSoftIrq
	statSteal
	statGuest
	statGuestNice
	statTotalBusy

	statNumCPUMetrics
)

type cpuMetrics [statNumCPUMetrics]*metric.Generic

func (m *cpuMetrics) Metrics() []metric.Metric {
	metrics := make([]metric.Metric, statNumCPUMetrics)
	for i, c := range m {
		metrics[i] = c
	}
	return metrics
}

const (
	statIntr = iota
	statCtxt
	statBTime
	statProcs
	statProcsRunning
	statProcsBlocked

	statNumOtherMetrics
)

type statCollector struct {
	instance     uint32
	opts         statOpts
	previous     *stat.Stat
	cpuMetrics   []cpuMetrics
	otherMetrics [statNumOtherMetrics]*metric.Generic

	// To transmit raw data to other collectors that depend on this data
	rawTransmitter *bus.MultiTransmitter
	// To tranmit the summary data
	*bus.SingleTransmitter
	collector.Poller
}

type statOpts struct {
	SampleRate time.Duration `prompt:"Sample Rate" json:"sampleRate"`
}

var statDefaultOpts = statOpts{
	SampleRate: 1 * time.Second,
}

func (c *statCollector) createCPUMetrics() {
	c.cpuMetrics = make([]cpuMetrics, numCPUs+1)

	maker := func(m *cpuMetrics, prefix string, instance uint32) {
		m[statUser] = metric.New(c, prefix+"User", prefix+"Percentage time spent in user space", instance, metric.Compare)
		m[statNice] = metric.New(c, prefix+"Nice", prefix+"Percentage time spent in user space nice'd processes", instance, metric.Compare)
		m[statSystem] = metric.New(c, prefix+"System", prefix+"Percentage time spent in kernel mode", instance, metric.Compare)
		m[statIdle] = metric.New(c, prefix+"Idle", prefix+"Percentage time spent idle", instance, metric.Compare)
		m[statIO] = metric.New(c, prefix+"I/O", prefix+"Percentage time spent performing I/O", instance, metric.Compare)
		m[statIrq] = metric.New(c, prefix+"IRQ", prefix+"Percentage time spent serving interrupts", instance, metric.Compare)
		m[statSoftIrq] = metric.New(c, prefix+"SoftIRQ", prefix+"Percentage time spent serving soft interrupts", instance, metric.Compare)
		m[statSteal] = metric.New(c, prefix+"OtherOS", prefix+"Percentage time spent in other virtualized operating systems", instance, metric.Compare)
		m[statGuest] = metric.New(c, prefix+"Guest", prefix+"Percentage time spent running a virtual CPU for guest operating systems", instance, metric.Compare)
		m[statGuestNice] = metric.New(c, prefix+"GuestNice", prefix+"Percentage time spent running a niced guest virtual CPU", instance, metric.Compare)
		m[statTotalBusy] = metric.New(c, prefix+"Usage", prefix+"Percentage time spent not idle", instance, metric.Compare)
	}

	maker(&c.cpuMetrics[0], "Total ", 0)
	for i := 0; i < numCPUs; i++ {
		maker(&c.cpuMetrics[i+1], "CPU ", uint32(i))
	}
}

func percent(busy time.Duration, duration time.Duration) float32 {
	if busy == 0 {
		return 0.0
	}
	return float32(float64(duration)/float64(busy)) * 100
}

func (c *statCollector) sendCPUMetrics(diff *stat.Stat, time time.Time) {

	for i := 0; i < len(c.cpuMetrics); i++ {
		total := diff.Total(i)
		totalBusy := diff.Busy(i)
		c.cpuMetrics[i][statUser].SendTimed(time, percent(total, diff.CPU[i].User))
		c.cpuMetrics[i][statNice].SendTimed(time, percent(total, diff.CPU[i].Nice))
		c.cpuMetrics[i][statSystem].SendTimed(time, percent(total, diff.CPU[i].System))
		c.cpuMetrics[i][statIdle].SendTimed(time, percent(total, diff.CPU[i].Idle))
		c.cpuMetrics[i][statIO].SendTimed(time, percent(total, diff.CPU[i].IO))
		c.cpuMetrics[i][statIrq].SendTimed(time, percent(total, diff.CPU[i].Irq))
		c.cpuMetrics[i][statSoftIrq].SendTimed(time, percent(total, diff.CPU[i].Softirq))
		c.cpuMetrics[i][statSteal].SendTimed(time, percent(total, diff.CPU[i].Steal))
		c.cpuMetrics[i][statGuest].SendTimed(time, percent(total, diff.CPU[i].Guest))
		c.cpuMetrics[i][statGuestNice].SendTimed(time, percent(total, diff.CPU[i].GuestNice))
		c.cpuMetrics[i][statTotalBusy].SendTimed(time, percent(total, totalBusy))
	}
}

func newStatCollector(instance uint32, stat *stat.Stat) *statCollector {
	var c statCollector

	opts := statDefaultOpts
	options.Get("client").Unmarshal("collectors."+StatCollectorName, &opts)

	c = statCollector{
		instance:          instance,
		previous:          stat,
		opts:              opts,
		rawTransmitter:    bus.NewMultiTransmitter(),
		SingleTransmitter: bus.NewSingleTransmitter(),
		Poller:            collector.NewPoller(opts.SampleRate, &c),
	}

	c.createCPUMetrics()
	c.otherMetrics[statIntr] = metric.New(c, "Interrupts", "Number of interrupts raised", 0, metric.Compare)
	c.otherMetrics[statCtxt] = metric.New(c, "Context Switches", "Number of context switches that have occured", 0, metric.Compare)
	c.otherMetrics[statBTime] = metric.New(c, "Boot Time", "Time when they system booted", 0, metric.Compare)
	c.otherMetrics[statProcs] = metric.New(c, "Num Processes", "Number of process currently started", 0, metric.Compare)
	c.otherMetrics[statProcsRunning] = metric.New(c, "Running Processes", "Number of process actively running", 0, metric.Compare)
	c.otherMetrics[statProcsBlocked] = metric.New(c, "Blocked Processes", "Number of process currently blocked (e.g. waiting on I/O)", 0, metric.Compare)

	return &c
}

// Name - Return the name of the Collector
func (c *statCollector) Name() string {
	return StatCollectorName
}

// Description - Return a brief description of the Collector
func (c *statCollector) Description() string {
	return statCollectorDesc
}

// Instance - Return theinstance of the Collector
func (c *statCollector) Instance() uint32 {
	return c.instance
}

// GetOpts - Return the current options for this Collector
func (c *statCollector) GetOpts() (interface{}, error) {
	return c.opts, nil
}

// SetOpts - Set the options for this Collector
func (c *statCollector) SetOpts(opts interface{}) error {
	var ok bool
	c.opts, ok = opts.(statOpts)
	if !ok {
		return fmt.Errorf("Invalid options format: %+v expected: %+v", opts, c.opts)
	}
	return nil
}

// Metrics - Return the slice of Metrics
func (c *statCollector) Metrics() []metric.Metric {
	var metrics []metric.Metric

	for i := 0; i < numCPUs; i++ {
		metrics = append(metrics, c.cpuMetrics[i].Metrics()...)
	}
	for i := 0; i < statNumOtherMetrics; i++ {
		metrics = append(metrics, c.otherMetrics[i])
	}
	return metrics
}

// Sample - Sample /proc/stat and send metrics
func (c *statCollector) Sample(t time.Time) error {
	log.Printf("StatCollector: Collecting...")
	s, err := stat.Get(nil)
	if err != nil {
		return err
	}

	diff := stat.Diff(c.previous, s)
	c.sendCPUMetrics(diff, t)
	c.otherMetrics[statIntr].SendTimed(t, diff.Intr)
	c.otherMetrics[statCtxt].SendTimed(t, diff.Ctxt)
	c.otherMetrics[statBTime].SendTimed(t, s.Btime)
	c.otherMetrics[statProcs].SendTimed(t, s.Procs)
	c.otherMetrics[statProcsRunning].SendTimed(t, s.ProcsRunning)
	c.otherMetrics[statProcsBlocked].SendTimed(t, s.ProcsBlocked)

	c.previous = s

	log.Printf("StatCollector: Done")
	return nil
}

// Prober:

type statProber struct{}

// Probe - Create the Collector
func (p *statProber) Probe(b bus.Bus) ([]bus.Attachment, error) {
	s, err := stat.Get(nil)
	if err != nil {
		return nil, err
	}

	a := []bus.Attachment{newStatCollector(0, s)}
	return a, nil
}

// Name - Name of the collector
func (p *statProber) Name() string {
	return StatCollectorName
}

// init:
var numCPUs int

func init() {
	s, err := stat.Get(nil)
	if err != nil {
		return
	}

	numCPUs = len(s.CPU) - 1
	collector.RegisterProber(&statProber{})
}
