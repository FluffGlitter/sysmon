# SysMon

SysMon started life as a personal project to learn golang while developing a simple monitoring system.

I still have this overall aim for the project, but it will likely be restricted to a distributed task manager initially (think windows task manager for all of your machines).

It is currently under development and should not be used by anyone for any serious reason yet.