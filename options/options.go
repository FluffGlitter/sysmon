package options

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/Jeffail/gabs"
)

// Options - represents configuration options
type Options struct {
	name string
	opts *gabs.Container
}

// Load - load options from a file
func (o *Options) Load(filename string) error {
	raw, err := os.Open(filename)
	if err != nil {
		log.Print("Unable to read Options [" + filename + "]: " + err.Error())
		return err
	}
	decoder := json.NewDecoder(raw)
	decoder.UseNumber()

	o.opts, err = gabs.ParseJSONDecoder(decoder)
	if err != nil {
		log.Print("Unable to load Options [" + filename + "]: " + err.Error())
		return err
	}

	return nil
}

// Save - save options to a file
func (o *Options) Save(filename string) error {
	raw := o.opts.StringIndent("", "\t")

	err := ioutil.WriteFile(filename, []byte(raw), 0640)
	if err != nil {
		log.Print("Unable to write Options [" + filename + "]: " + err.Error())
		return err
	}

	return nil
}

// Get - Get an option based on string key
func (o *Options) Get(key string) (interface{}, error) {
	if !o.opts.ExistsP(key) {
		return nil, fmt.Errorf("No such Options [%v:%v]", o.name, key)
	}

	return o.opts.Path(key).Data(), nil
}

// GetString - Get a string options based on string key
// Returns empty string on error
func (o *Options) GetString(key string) string {
	i, err := o.Get(key)
	if err != nil {
		return ""
	}

	s, ok := i.(string)
	if !ok {
		return ""
	}

	return s
}

// GetJSON - Get a JSON encoded option based on string key
func (o *Options) GetJSON(key string) string {
	if !o.opts.ExistsP(key) {
		return ""
	}

	return o.opts.Path(key).String()
}

// Set - Set an option based on string key and value
func (o *Options) Set(key string, val interface{}) error {
	o.opts.SetP(val, key)
	return nil
}

// Unmarshal - Unmarshal an option to a struct
func (o *Options) Unmarshal(key string, val interface{}) error {
	raw := o.GetJSON(key)
	if raw == "" {
		return fmt.Errorf("No such Options [%v:%v]", o.name, key)
	}

	return json.Unmarshal([]byte(raw), val)
}

// New - create a new Options struct
func New(name string, data []byte) *Options {
	o := &Options{
		name: name,
		opts: gabs.New(),
	}
	if data != nil {
		var err error
		o.opts, err = gabs.ParseJSON(data)
		if err != nil {
			log.Print("Unable to initialize Options [" + name + "with data [" + string(data) + "]: " + err.Error())
			return nil
		}
	}

	return o
}

var globalOpts map[string]*Options

func init() {
	globalOpts = make(map[string]*Options)
}

// Register - Register an Options for global lookup
func Register(o *Options) error {
	_, ok := globalOpts[o.name]
	if ok {
		return fmt.Errorf("Global Options [" + o.name + "] already exists")
	}

	globalOpts[o.name] = o
	return nil
}

// Get - Retrieve a previously registerd global Options
func Get(name string) *Options {
	v, ok := globalOpts[name]
	if !ok {
		return &Options{name: name}
	}

	return v
}
