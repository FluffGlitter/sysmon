package endpoint

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"io"
	"io/ioutil"
	"net"
	"net/http"

	"gitlab.com/FluffGlitter/sysmon/server/api"
	"gitlab.com/FluffGlitter/sysmon/server/appctx"
	"gitlab.com/FluffGlitter/sysmon/server/client"
)

// GetRegisterHandler - Returns a handler for client registration
func GetRegisterHandler(ctx *appctx.AppCtx) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
			return
		}
		// read the data from the request
		body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1*1024*1024))
		if err != nil {
			http.Error(w, "Invalid request", http.StatusBadRequest)
		}

		// convert to info
		var info api.Register
		err = json.Unmarshal(body, &info)
		if err != nil {
			http.Error(w, "Invalid request", http.StatusBadRequest)
		}

		// Register a client (or retrieve an existing one that matches)
		c, err := client.Register(ctx, &info, net.ParseIP(r.RemoteAddr))
		if c == nil {
			http.Error(w, "Unable to register: "+err.Error(), http.StatusInternalServerError)
		}

		// convert servers key
		keydata := bytes.NewBuffer(nil)
		enc := gob.NewEncoder(keydata)
		err = enc.Encode(ctx.Key.PublicKey)
		if err != nil {
			http.Error(w, "Unable to send key: "+err.Error(), http.StatusInternalServerError)
		}

		// construct the response
		resp := api.RegisterResponse{Key: keydata.Bytes()}

		// write the response
		respdata, err := json.Marshal(resp)
		if err != nil {
			http.Error(w, "Unable to encode response: "+err.Error(), http.StatusInternalServerError)
		}
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		w.Write(respdata)
	})
}
