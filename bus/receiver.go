package bus

import (
	"fmt"

	"gitlab.com/FluffGlitter/sysmon/bus/message"
)

// Receiver - Receives a Sample from a Bus
type Receiver interface {
	Attachment
	message.Receiver
}

// One2OneReceiver - Receive from a single Bus and propogate to a Receiver
type One2OneReceiver struct {
	bus      Bus
	receiver message.Receiver
}

// Attach - Attach to the given Bus
func (r *One2OneReceiver) Attach(bus Bus) error {
	if r.bus != nil {
		return fmt.Errorf("Unable to attach to bus [%v]. Already attached to [%v]", bus, r.bus)
	}
	r.bus = bus
	return nil
}

// Detach - Detach from the given Bus
func (r *One2OneReceiver) Detach(bus Bus) error {
	if r.bus != bus {
		return fmt.Errorf("Unable to detach from [%v]. Currently attached to [%v]", bus, r.bus)
	}
	r.bus = nil
	return nil
}

// Receive - Recevie a message from the Bus
func (r *One2OneReceiver) Receive(m message.Message) error {
	if r.receiver == nil {
		return fmt.Errorf("No receiver")
	}
	return r.receiver.Receive(m)
}

// NewOne2OneReceiver - Return a new One2OneReceiver
func NewOne2OneReceiver(r message.Receiver) *One2OneReceiver {
	return &One2OneReceiver{receiver: r}
}

// Many2OneReceiver - Receive from any attached Bus
type Many2OneReceiver struct {
	buses    map[Bus]bool
	receiver message.Receiver
}

// Attach - Attach to the given Bus
func (r *Many2OneReceiver) Attach(bus Bus) error {
	if r.buses[bus] {
		return fmt.Errorf("Alreay attached to [%v]", bus)
	}

	r.buses[bus] = true
	return nil
}

// Detach - Detach from the given Bus
func (r *Many2OneReceiver) Detach(bus Bus) error {
	if !r.buses[bus] {
		return fmt.Errorf("Not attached to [%v]", bus)
	}

	delete(r.buses, bus)
	return nil
}

// Receive - Receive a message from any of the Buses
func (r *Many2OneReceiver) Receive(m message.Message) error {
	if r.receiver == nil {
		return fmt.Errorf("No receiver")
	}

	return r.receiver.Receive(m)
}

// NewMany2OneReceiver - Return a new Many2OneReceiver
func NewMany2OneReceiver(r message.Receiver) *Many2OneReceiver {
	return &Many2OneReceiver{receiver: r}
}
