package collectors

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/FluffGlitter/sysmon/bus"
	"gitlab.com/FluffGlitter/sysmon/collector"
	"gitlab.com/FluffGlitter/sysmon/metric"
	"gitlab.com/FluffGlitter/sysmon/options"
	"golang.org/x/sys/unix"
)

const sysInfoCollectorName = "SysInfo"
const sysInfoCollectorDesc = "Collector to capture info about the running system as reported by sysinfo syscall"

const (
	siUptime = iota
	siLoad1
	siLoad5
	siLoad15
	siTotalRAM
	siFreeRAM
	siSharedRAM
	siBufferRAM
	siTotalSwap
	siFreeSwap
	siProcs
	siTotalHighMem
	siFreeHighMem

	// Must be last in list
	siNumMetrics
)

type sysInfoCollector struct {
	instance uint32
	opts     sysInfoOptions
	previous *unix.Sysinfo_t
	metrics  []*metric.Generic
	*bus.SingleTransmitter
	collector.Poller
}

type sysInfoOptions struct {
	SampleRate time.Duration `prompt:"Sample Rate"`
}

var sysInfoDefaultOpts = sysInfoOptions{
	SampleRate: 1 * time.Second,
}

func newSysInfoCollector(instance uint32) *sysInfoCollector {
	var s sysInfoCollector

	opts := sysInfoDefaultOpts
	options.Get("client").Unmarshal("collectors."+sysInfoCollectorName, &opts)

	s = sysInfoCollector{
		instance:          instance,
		opts:              opts,
		metrics:           make([]*metric.Generic, siNumMetrics),
		SingleTransmitter: bus.NewSingleTransmitter(),
		Poller:            collector.NewPoller(opts.SampleRate, &s),
	}

	s.metrics[siUptime] = metric.New(s, "Uptime", "Time since boot (in seconds)", 0, metric.Compare)
	s.metrics[siLoad1] = metric.New(s, "Load Avg (1 Minute)", "Load average over the previous minute", 0, metric.Compare)
	s.metrics[siLoad5] = metric.New(s, "Load Avg (5 Minute)", "Load average over the previous 5 minutes", 0, metric.Compare)
	s.metrics[siLoad15] = metric.New(s, "Load Avg (15 Minute)", "Load average over the previous 15 minutes", 0, metric.Compare)
	s.metrics[siTotalRAM] = metric.New(s, "Total RAM", "Total RAM available to OS", 0, metric.Compare)
	s.metrics[siFreeRAM] = metric.New(s, "Free RAM", "Free amount of RAM available for use", 0, metric.Compare)
	s.metrics[siSharedRAM] = metric.New(s, "Shared RAM", "Amount of RAM used for shared memory", 0, metric.Compare)
	s.metrics[siBufferRAM] = metric.New(s, "Buffer RAM", "Amount of RAM used as buffers", 0, metric.Compare)
	s.metrics[siTotalSwap] = metric.New(s, "Total Swap", "Total amount of swap space available to the OS", 0, metric.Compare)
	s.metrics[siFreeSwap] = metric.New(s, "Free Swap", "Amount of swap space currently free", 0, metric.Compare)
	s.metrics[siProcs] = metric.New(s, "Num Procs", "Nuber of processes currently running", 0, metric.Compare)
	s.metrics[siTotalHighMem] = metric.New(s, "Total HighMem", "Total amount of High memory available to the system", 0, metric.Compare)
	s.metrics[siFreeHighMem] = metric.New(s, "Free HighMem", "Amount of High memory currently free for use", 0, metric.Compare)

	return &s
}

// Name - Return the name of the Collector
func (c *sysInfoCollector) Name() string {
	return sysInfoCollectorName
}

// Description - Return a brief description of the Collector
func (c *sysInfoCollector) Description() string {
	return sysInfoCollectorDesc
}

// Instance - Return theinstance of the Collector
func (c *sysInfoCollector) Instance() uint32 {
	return c.instance
}

// GetOpts - Return the current options for this Collector
func (c *sysInfoCollector) GetOpts() (interface{}, error) {
	return c.opts, nil
}

// SetOpts - Set the options for this Collector
func (c *sysInfoCollector) SetOpts(opts interface{}) error {
	var ok bool
	c.opts, ok = opts.(sysInfoOptions)
	if !ok {
		return fmt.Errorf("Invalid options format: %+v expected: %+v", opts, c.opts)
	}
	return nil
}

// Metrics - Return the slice of Metrics
func (c *sysInfoCollector) Metrics() []metric.Metric {
	metrics := make([]metric.Metric, siNumMetrics)

	for i, m := range c.metrics {
		metrics[i] = m
	}

	return metrics
}

// Sample - Sample the sysinfo and send metrics
func (c *sysInfoCollector) Sample(t time.Time) error {
	var si unix.Sysinfo_t
	err := unix.Sysinfo(&si)
	if err != nil {
		return err
	}

	c.metrics[siUptime].SendTimed(t, si.Uptime)
	c.metrics[siLoad1].SendTimed(t, si.Loads[0])
	c.metrics[siLoad5].SendTimed(t, si.Loads[1])
	c.metrics[siLoad15].SendTimed(t, si.Loads[2])
	c.metrics[siTotalRAM].SendTimed(t, si.Totalram)
	c.metrics[siFreeRAM].SendTimed(t, si.Freeram)
	c.metrics[siSharedRAM].SendTimed(t, si.Sharedram)
	c.metrics[siBufferRAM].SendTimed(t, si.Bufferram)
	c.metrics[siTotalSwap].SendTimed(t, si.Totalswap)
	c.metrics[siFreeSwap].SendTimed(t, si.Freeswap)
	c.metrics[siProcs].SendTimed(t, si.Procs)
	c.metrics[siTotalHighMem].SendTimed(t, si.Totalhigh)
	c.metrics[siFreeHighMem].SendTimed(t, si.Freehigh)

	return nil
}

type sysInfoProber struct {
}

// Probe - Create the Collectors
func (p *sysInfoProber) Probe(b bus.Bus) ([]bus.Attachment, error) {
	// make a sysinfo call to check we can use it
	var si unix.Sysinfo_t
	err := unix.Sysinfo(&si)
	if err != nil {
		return nil, err
	}

	// we only support 1 sysInfoCollector connected to any bus
	a := append([]bus.Attachment{}, newSysInfoCollector(1))

	return a, nil
}

// Name - Name of the Prober
func (p *sysInfoProber) Name() string {
	return sysInfoCollectorName
}

func init() {
	err := collector.RegisterProber(&sysInfoProber{})
	if err != nil {
		log.Fatalf("Unable to register sysInfoCollector: %v", err)
	}
}
