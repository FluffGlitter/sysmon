package descriptor

// Descriptor - Describes a Metric
type Descriptor interface {
	// Name - Returns the name of the Metric
	// Used for informational purposes.
	Name() string

	// Description - Returns a description of the specific Metric
	// Used for inforational purposes.
	Description() string

	// Instance - Returns the instance number of this Metric
	// Used for informational purposes. e.g. 4 Metrics with consecutive Instance values could
	// be used to monitor a quad core CPU load.
	Instance() uint32
}
