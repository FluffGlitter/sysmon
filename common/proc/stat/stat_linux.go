package stat

import (
	"bufio"
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/FluffGlitter/sysmon/common/proc"
)

// CPU - Represents the CPU data in /proc/stat.
type CPU struct {
	// Time spent in user mode
	User time.Duration
	// Time spent in user mode with low priority
	Nice time.Duration
	// Time spent in system mode
	System time.Duration
	// Time spent in idel task
	Idle time.Duration
	// Time spent waiting for I/O to complete
	IO time.Duration
	// Time spent servicing interrupts
	Irq time.Duration
	// Time spent servicing soft interrupts
	Softirq time.Duration
	// Time spent in other OSs (i.e. other virtualized OSs)
	Steal time.Duration
	// Time spent running a virtual CPU for guest of this kernel
	Guest time.Duration
	// Time spent running a niced guest
	GuestNice time.Duration
}

// Stat - Represents the data in /proc/stat.
type Stat struct {
	// CPU status per processor. [0]=Total
	CPU []CPU
	// Number of interrupts serviced
	Intr uint64
	// Number of context switches
	Ctxt uint64
	// Time when system booted
	Btime time.Time
	// Number of processes started since booted
	Procs uint64
	// Number of processes currently active
	ProcsRunning uint64
	// Number of processes currently blocked on I/O
	ProcsBlocked uint64
}

// Busy - Return the total duration of all CPU times excluding Idle
func (s *Stat) Busy(cpu int) time.Duration {
	totalBusy := s.CPU[cpu].User
	totalBusy += s.CPU[cpu].Nice
	totalBusy += s.CPU[cpu].System
	totalBusy += s.CPU[cpu].IO
	totalBusy += s.CPU[cpu].Irq
	totalBusy += s.CPU[cpu].Softirq
	totalBusy += s.CPU[cpu].Steal
	totalBusy += s.CPU[cpu].Guest
	totalBusy += s.CPU[cpu].GuestNice

	return totalBusy
}

// Total - Return the total duration of busy and idle
func (s *Stat) Total(cpu int) time.Duration {
	return s.CPU[cpu].Idle + s.Busy(cpu)
}

func min(i, j int) int {
	if i < j {
		return i
	}
	return j
}

// Diff - Return the delta between first and second
func Diff(first, second *Stat) *Stat {
	ncpu := min(len(first.CPU), len(second.CPU))
	s := &Stat{CPU: make([]CPU, ncpu)}

	for i := 0; i < ncpu; i++ {
		s.CPU[i].User = second.CPU[i].User - first.CPU[i].User
		s.CPU[i].Nice = second.CPU[i].Nice - first.CPU[i].Nice
		s.CPU[i].System = second.CPU[i].System - first.CPU[i].System
		s.CPU[i].Idle = second.CPU[i].Idle - first.CPU[i].Idle
		s.CPU[i].IO = second.CPU[i].IO - first.CPU[i].IO
		s.CPU[i].Irq = second.CPU[i].Irq - first.CPU[i].Irq
		s.CPU[i].Softirq = second.CPU[i].Softirq - first.CPU[i].Softirq
		s.CPU[i].Steal = second.CPU[i].Steal - first.CPU[i].Steal
		s.CPU[i].Guest = second.CPU[i].User - first.CPU[i].User
		s.CPU[i].GuestNice = second.CPU[i].GuestNice - first.CPU[i].GuestNice
	}
	s.Intr = second.Intr - first.Intr
	s.Ctxt = second.Ctxt - first.Ctxt
	s.Btime = first.Btime
	s.Procs = second.Procs - first.Procs
	s.ProcsRunning = second.ProcsRunning - first.ProcsRunning
	s.ProcsBlocked = second.ProcsBlocked - first.ProcsBlocked

	return s
}

func fillCPU(cpu *CPU, str []string) {
	if len(str) != 11 {
		return
	}

	cpu.User = proc.UserHzDuration(str[1])
	cpu.Nice = proc.UserHzDuration(str[2])
	cpu.System = proc.UserHzDuration(str[3])
	cpu.Idle = proc.UserHzDuration(str[4])
	cpu.IO = proc.UserHzDuration(str[5])
	cpu.Irq = proc.UserHzDuration(str[6])
	cpu.Softirq = proc.UserHzDuration(str[7])
	cpu.Steal = proc.UserHzDuration(str[8])
	cpu.Guest = proc.UserHzDuration(str[9])
	cpu.GuestNice = proc.UserHzDuration(str[10])
}

//Get - Return (or fill) the system wide stat info
func Get(stat *Stat) (*Stat, error) {
	f, err := proc.Open("stat")
	if err != nil {
		return nil, err
	}
	defer f.Close()

	if stat == nil {
		stat = &Stat{}
	}

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		toks := strings.Fields(line)

		if strings.HasPrefix(toks[0], "cpu") {
			var num int
			n, _ := fmt.Sscanf(toks[0], "cpu%d", &num)
			if n == 0 {
				num = 0
			} else {
				num++
			}
			if len(stat.CPU) < (num + 1) {
				stat.CPU = append(stat.CPU,
					make([]CPU, num+1-len(stat.CPU))...)
			}
			fillCPU(&stat.CPU[num], toks)
		} else {
			switch toks[0] {
			case "intr":
				stat.Intr, _ = strconv.ParseUint(toks[1], 10, 64)
			case "ctxt":
				stat.Ctxt, _ = strconv.ParseUint(toks[1], 10, 64)
			case "btime":
				booted, _ := strconv.ParseUint(toks[1], 10, 64)
				stat.Btime = time.Date(1970, time.January, 1, 0, 0, 0, 0, time.Local)
				stat.Btime = stat.Btime.Add(time.Duration(booted) * time.Second)
			case "processes":
				stat.Procs, _ = strconv.ParseUint(toks[1], 10, 64)
			case "procs_running":
				stat.ProcsRunning, _ = strconv.ParseUint(toks[1], 10, 64)
			case "procs_blocked":
				stat.ProcsBlocked, _ = strconv.ParseUint(toks[1], 10, 64)
			}
		}
	}

	return stat, nil
}
