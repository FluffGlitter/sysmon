package metric

import (
	"time"

	"gitlab.com/FluffGlitter/sysmon/bus/message"
	"gitlab.com/FluffGlitter/sysmon/metric/descriptor"
	"gitlab.com/FluffGlitter/sysmon/sample"
)

//Metric - Interface to any Metric
type Metric interface {
	descriptor.Descriptor
}

//Generic - Generic Metric to send anything via interface{}
type Generic struct {
	name        string
	description string
	instance    uint32
	message.Transmitter

	previous interface{}
	equal    func(v1 interface{}, v2 interface{}) bool
}

// Name - Returns the name of the Metric
func (g *Generic) Name() string {
	return g.name
}

// Description - Returns the descritpions of the Metric
func (g *Generic) Description() string {
	return g.description
}

// Instance - Returns the instance number of the Metric
func (g *Generic) Instance() uint32 {
	return g.instance
}

func (g *Generic) dedup(v interface{}) bool {
	if (g.equal != nil) && (g.previous != nil) {
		return g.equal(g.previous, v)
	}
	return false
}

func (g *Generic) sendSample(s sample.Sample) {
	g.Transmit(s)
	g.previous = s.Data()
}

// Send - Sends a value
func (g *Generic) Send(v interface{}) {
	if g.dedup(v) {
		return
	}
	g.sendSample(sample.New(time.Now(), g, v))
}

// SendTimed - Sends a value using the given time
func (g *Generic) SendTimed(t time.Time, v interface{}) {
	if g.dedup(v) {
		return
	}
	g.sendSample(sample.New(t, g, v))
}

// SendSample - Sends a preconstructed sample
// e.g. if you want to ensure the same timestamp for multiple metrics
func (g *Generic) SendSample(s sample.Sample) {
	if g.dedup(s.Data()) {
		return
	}
	g.sendSample(s)
}

// New - Returns a new Generic Metric
func New(transmitter message.Transmitter, name string, description string, instance uint32,
	compare func(interface{}, interface{}) bool) *Generic {
	return &Generic{
		name:        name,
		description: description,
		instance:    instance,
		previous:    nil,
		equal:       compare,
		Transmitter: transmitter,
	}
}

// Compare - compare 2 values of same type
func Compare(i1 interface{}, i2 interface{}) bool {
	return (i1 != nil) && (i1 == i2)
}
