package db

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"github.com/influxdata/influxdb/coordinator"
	"github.com/influxdata/influxdb/influxql"
	"github.com/influxdata/influxdb/models"
	"github.com/influxdata/influxdb/monitor"
	"github.com/influxdata/influxdb/services/meta"
	"github.com/influxdata/influxdb/services/precreator"
	"github.com/influxdata/influxdb/services/retention"
	"github.com/influxdata/influxdb/toml"
	"github.com/influxdata/influxdb/tsdb"
	"github.com/uber-go/zap"
	"gitlab.com/FluffGlitter/sysmon/sample"
	// Initialize the engine packages
	_ "github.com/influxdata/influxdb/tsdb/engine"
)

const (
	sampleDBName        = "Samples"
	sampleRetentionName = "SamplesRetention"
)

type service interface {
	WithLogger(log zap.Logger)
	Open() error
	Close() error
}

type config struct {
	Meta        *meta.Config       `toml:"meta"`
	Data        tsdb.Config        `toml:"data"`
	Coordinator coordinator.Config `toml:"coordinator"`
	Retention   retention.Config   `toml:"retention"`
	Precreator  precreator.Config  `toml:"shard-precreation"`
	Monitor     monitor.Config     `toml:"monitor"`
}

func newConfig() *config {
	return &config{
		Meta:        meta.NewConfig(),
		Data:        tsdb.NewConfig(),
		Coordinator: coordinator.NewConfig(),
		Retention:   retention.NewConfig(),
		Precreator:  precreator.NewConfig(),
		Monitor:     monitor.NewConfig(),
	}
}

// LocalDB - Represents a local database
type LocalDB struct {
	metaClient *meta.Client
	store      *tsdb.Store
	executor   *influxql.QueryExecutor
	writer     *coordinator.PointsWriter
	services   []service
	monitor    *monitor.Monitor
	config     *config
	logger     zap.Logger
	logFile    *os.File
}

// NewLocalDB - Create a new local tsdb db
func NewLocalDB() (*LocalDB, error) {
	d := &LocalDB{
		config: newConfig(),
	}

	// Config
	// TODO: read this from a file
	d.config.Meta.Dir, _ = filepath.Abs("influxdb/meta")
	d.config.Meta.RetentionAutoCreate = true
	d.config.Meta.LoggingEnabled = false

	d.config.Data.Dir, _ = filepath.Abs("influxdb/data")
	d.config.Data.WALDir, _ = filepath.Abs("./influxdb/wal")

	os.MkdirAll(d.config.Meta.Dir, 0755)
	os.MkdirAll(d.config.Data.Dir, 0755)
	os.MkdirAll(d.config.Data.WALDir, 0755)

	d.config.Retention.Enabled = true
	d.config.Retention.CheckInterval = toml.Duration(30) * toml.Duration(time.Minute)

	d.config.Precreator.Enabled = true
	d.config.Precreator.CheckInterval = toml.Duration(10) * toml.Duration(time.Minute)
	d.config.Precreator.AdvancePeriod = toml.Duration(30) * toml.Duration(time.Minute)

	d.config.Monitor.StoreEnabled = true
	d.config.Monitor.StoreDatabase = "_internal"
	d.config.Monitor.StoreInterval = toml.Duration(10) * toml.Duration(time.Second)

	// meta data client
	d.metaClient = meta.NewClient(d.config.Meta)

	// monitor
	d.monitor = monitor.New(d, d.config.Monitor)

	// store
	d.store = tsdb.NewStore(d.config.Data.Dir)
	d.store.EngineOptions.Config = d.config.Data

	// writer
	d.writer = coordinator.NewPointsWriter()
	d.writer.WriteTimeout = time.Duration(d.config.Coordinator.WriteTimeout)
	d.writer.TSDBStore = d.store

	// executor
	d.executor = influxql.NewQueryExecutor()
	d.executor.StatementExecutor = &coordinator.StatementExecutor{
		MetaClient:  d.metaClient,
		TaskManager: d.executor.TaskManager,
		TSDBStore:   coordinator.LocalTSDBStore{Store: d.store},
		ShardMapper: &coordinator.LocalShardMapper{
			MetaClient: d.metaClient,
			TSDBStore:  d.store,
		},
		Monitor:           d.monitor,
		PointsWriter:      d.writer,
		MaxSelectPointN:   d.config.Coordinator.MaxSelectPointN,
		MaxSelectSeriesN:  d.config.Coordinator.MaxSelectSeriesN,
		MaxSelectBucketsN: d.config.Coordinator.MaxSelectBucketsN,
	}
	d.executor.TaskManager.QueryTimeout = time.Duration(d.config.Coordinator.QueryTimeout)
	d.executor.TaskManager.LogQueriesAfter = time.Duration(d.config.Coordinator.LogQueriesAfter)
	d.executor.TaskManager.MaxConcurrentQueries = d.config.Coordinator.MaxConcurrentQueries

	// monitor
	d.monitor.Version = "unknown"
	d.monitor.Commit = "unknown"
	d.monitor.Branch = "unknown"
	d.monitor.PointsWriter = (*monitorPointsWriter)(d.writer)

	return d, nil
}

// Statistics returns statistics for the services running in the Server.
func (db *LocalDB) Statistics(tags map[string]string) []models.Statistic {
	var statistics []models.Statistic
	statistics = append(statistics, db.executor.Statistics(tags)...)
	statistics = append(statistics, db.store.Statistics(tags)...)
	statistics = append(statistics, db.writer.Statistics(tags)...)
	for _, srv := range db.services {
		if m, ok := srv.(monitor.Reporter); ok {
			statistics = append(statistics, m.Statistics(tags)...)
		}
	}
	return statistics
}

type monitorPointsWriter coordinator.PointsWriter

func (pw *monitorPointsWriter) WritePoints(database, retentionPolicy string, points models.Points) error {
	return (*coordinator.PointsWriter)(pw).WritePoints(database, retentionPolicy, models.ConsistencyLevelAny, points)
}

// Open - Open the database
func (db *LocalDB) Open() error {
	var err error
	// Logging
	// TODO: get db log file from an options
	db.logFile, err = os.Create("./influxdb/db.log")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: Unable to open log file: %v", err)
		return err
	}
	db.logger = zap.New(zap.NewTextEncoder(), zap.Output(db.logFile))

	if err := db.metaClient.Open(); err != nil {
		db.logger.Fatal(fmt.Sprintf("Unable to open metaClient: %v", err))
		return err
	}

	// Monitor
	db.services = append(db.services, db.monitor)

	// Precreator
	if db.config.Precreator.Enabled {
		srv, err := precreator.NewService(db.config.Precreator)
		if err != nil {
			return err
		}
		srv.MetaClient = db.metaClient
		db.services = append(db.services, srv)
	}

	// RetentionPolicy
	if db.config.Retention.Enabled {
		srv := retention.NewService(db.config.Retention)
		srv.MetaClient = db.metaClient
		srv.TSDBStore = db.store
		db.services = append(db.services, srv)
	}

	// Set metaClient
	db.writer.MetaClient = db.metaClient
	db.monitor.MetaClient = db.metaClient

	// Set logger for services
	if db.config.Meta.LoggingEnabled {
		db.metaClient.WithLogger(db.logger)
	}
	db.store.WithLogger(db.logger)
	if db.config.Data.QueryLogEnabled {
		db.executor.WithLogger(db.logger)
	}
	db.writer.WithLogger(db.logger)
	for _, srv := range db.services {
		srv.WithLogger(db.logger)
	}
	db.monitor.WithLogger(db.logger)

	// Open the store
	if err := db.store.Open(); err != nil {
		return err
	}

	// Open the writer
	if err := db.writer.Open(); err != nil {
		return err
	}

	// Open the services
	for _, srv := range db.services {
		if err := srv.Open(); err != nil {
			return err
		}
	}

	if err := db.Create(); err != nil {
		return err
	}

	return nil
}

// Close - Close the database
func (db *LocalDB) Close() error {
	for _, srv := range db.services {
		srv.Close()
	}

	if db.writer != nil {
		db.writer.Close()
	}

	if db.executor != nil {
		db.executor.Close()
	}

	if db.store != nil {
		db.store.Close()
	}

	if db.metaClient != nil {
		db.metaClient.Close()
	}

	db.services = nil
	db.logFile.Close()

	return nil
}

// Create - Create the database if needed
func (db *LocalDB) Create() error {
	duration := time.Duration(48 * time.Hour)
	replicaN := 1
	spec := meta.RetentionPolicySpec{
		Name:               sampleRetentionName,
		Duration:           &duration,
		ReplicaN:           &replicaN,
		ShardGroupDuration: 24 * time.Hour,
	}

	info, err := db.metaClient.CreateDatabaseWithRetentionPolicy(sampleDBName, &spec)
	if err != nil {
		return err
	}

	db.logger.Debug(fmt.Sprintf("Database [\"Samples\"]: %v", info))

	return nil
}

// StoreSample - Store a sample in the database
func (db *LocalDB) StoreSample(s sample.Sample) error {
	// TODO: batch points up for better performance
	hostname, _ := os.Hostname()

	p, err := models.NewPoint(
		s.Metric().Name(),
		models.NewTags(map[string]string{
			"instance": strconv.FormatUint(uint64(s.Metric().Instance()), 10),
			"host":     hostname,
			"desc":     s.Metric().Description(),
		}),
		map[string]interface{}{
			"value": s.Data(),
		},
		s.Timestamp(),
	)
	if err != nil {
		db.logger.Error(fmt.Sprintf("StoreSample: Error: s=[%v] err=%v", s, err))
		return err
	}

	err = db.writer.WritePoints(sampleDBName, "",
		models.ConsistencyLevelAny, []models.Point{p})
	if err != nil {
		db.logger.Error(fmt.Sprintf("StoreSample: Error writing points: %v", err))
	}

	return nil
}
