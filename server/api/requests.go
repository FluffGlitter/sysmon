package api

import uuid "github.com/satori/go.uuid"

// Register - Data used to register a client with the collection server
type Register struct {
	// Uniquely identifies a client
	UUID uuid.UUID `json:"uuid"`
	// The Name of the client (usually this would be the hostname)
	Name string `json:"name"`
	// Public key that the server should use when communicating to the client
	Key []byte `json:"key"`
}

// RegisterResponse - Data used to respond to a Register request
type RegisterResponse struct {
	// Public key that the client should use when communicating to the server
	Key []byte
}
