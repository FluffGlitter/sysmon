package main

import (
	"log"
	"net/http"
	"path/filepath"

	"gitlab.com/FluffGlitter/sysmon/options"
	"gitlab.com/FluffGlitter/sysmon/server/api/endpoint"
	"gitlab.com/FluffGlitter/sysmon/server/appctx"
)

const (
	servePath = "server.serve"
)

func main() {
	opts := options.New("server", nil)
	err := opts.Load(filepath.FromSlash("config/server.conf"))
	if err != nil {
		opts = options.New("server", []byte(`
			{ 
				"datastore": {
					"provider": "boltdb",
					"location": "boltdb/data.db"
				},
				"server": {
					"serve": ":8081"
				}
			}`))
	}
	options.Register(opts)

	ctx := appctx.New()
	defer ctx.Close()

	http.Handle("/api/v1/register", endpoint.GetRegisterHandler(ctx))

	serving := options.Get("server").GetString(servePath)
	if serving == "" {
		log.Fatalf("Configuration Error: %v not speified", servePath)
	}
	http.ListenAndServe(serving, nil)
}
