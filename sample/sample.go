package sample

import (
	"time"

	"gitlab.com/FluffGlitter/sysmon/bus/message"
	"gitlab.com/FluffGlitter/sysmon/metric/descriptor"
)

// Sample - Generic interface for a sample provided b a Metric
type Sample interface {
	message.Message
	// Metric - Returns the Metric this sample was generated for
	Metric() descriptor.Descriptor
}

// Generic - Sample structure used for any concrete type where
// operations are not needed for interpretation
type Generic struct {
	timestamp time.Time
	metric    descriptor.Descriptor
	data      interface{}
}

// Timestamp - Returns the time that this sample was collected
func (s *Generic) Timestamp() time.Time {
	return s.timestamp
}

// Data - Returns the data for this sample
func (s *Generic) Data() interface{} {
	return s.data
}

// Metric - Returns the Metric that this sample is for
func (s *Generic) Metric() descriptor.Descriptor {
	return s.metric
}

// New - Returns a new GenericSample as a Sample interface
// containing the passed in data
func New(timestamp time.Time, descriptor descriptor.Descriptor, data interface{}) Sample {
	return &Generic{
		timestamp: timestamp,
		metric:    descriptor,
		data:      data,
	}
}
