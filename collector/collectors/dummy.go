package collectors

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/FluffGlitter/sysmon/bus"
	"gitlab.com/FluffGlitter/sysmon/collector"
	"gitlab.com/FluffGlitter/sysmon/metric"
	"gitlab.com/FluffGlitter/sysmon/options"
)

const defaultNumDummies uint = 100
const dummyName = "Dummy"
const dummyDesc = "Dummy Collector that continuously sends a constant"

type dummy struct {
	instance uint32
	metric   *metric.Generic
	*bus.SingleTransmitter
	collector.Poller
}

func newDummy(instance uint32) *dummy {
	var d dummy
	d = dummy{
		instance:          instance,
		metric:            metric.New(d, "Value", "Constant Value", instance, nil),
		SingleTransmitter: bus.NewSingleTransmitter(),
		Poller:            collector.NewConstantPoller(&d),
	}
	return &d
}

func (c *dummy) Name() string {
	return dummyName
}

func (c *dummy) Description() string {
	return dummyDesc
}

func (c *dummy) Instance() uint32 {
	return c.instance
}

func (c *dummy) GetOpts() (interface{}, error) {
	return nil, fmt.Errorf("No options for dummy Collector")
}

func (c *dummy) SetOpts(opts interface{}) error {
	return fmt.Errorf("No options should be set for dummy Collector")
}

func (c *dummy) Metrics() []metric.Metric {
	metrics := make([]metric.Metric, 1)
	metrics[0] = c.metric

	return metrics
}

func (c *dummy) Sample(t time.Time) error {
	c.metric.SendTimed(t, c.instance)

	return nil
}

type dummyProber struct {
}

//Probe - Create the dummies
func (p *dummyProber) Probe(b bus.Bus) ([]bus.Attachment, error) {
	num := defaultNumDummies
	n, _ := options.Get("client").Get("collectors.Dummy.NumDummies")
	num, ok := n.(uint)
	if !ok {
		return nil, fmt.Errorf("Invalid NumDummys [%v]", n)
	}

	dummies := []bus.Attachment{}
	for i := uint(0); i < num; i++ {
		dummies = append(dummies, newDummy(uint32(i)))
	}

	return dummies, nil
}

// Name - Name of the Prober
func (p *dummyProber) Name() string {
	return dummyName
}

func init() {
	err := collector.RegisterProber(&dummyProber{})
	if err != nil {
		log.Fatalf("Unable to register dummyProber: %v", err)
	}
}
