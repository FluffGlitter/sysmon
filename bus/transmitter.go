package bus

import (
	"fmt"

	"gitlab.com/FluffGlitter/sysmon/bus/message"
)

// Transmitter - Receives a mesage and Transmits it via a Bus
type Transmitter interface {
	Attachment
	message.Transmitter
}

// SingleTransmitter - Transmit across a single Bus
type SingleTransmitter struct {
	bus Bus
}

// Attach - Attach to the given Bus
func (t *SingleTransmitter) Attach(bus Bus) error {
	if t.bus != nil {
		return fmt.Errorf("Unable to attach to bus [%v]. Already attached to [%v]", bus, t.bus)
	}
	t.bus = bus
	return nil
}

// Detach - Detach from the given Bus
func (t *SingleTransmitter) Detach(bus Bus) error {
	if t.bus != bus {
		return fmt.Errorf("Unable to detach from [%v]. Currently attached to [%v]", bus, t.bus)
	}
	t.bus = nil
	return nil
}

// Transmit - Send the message to the Bus
func (t *SingleTransmitter) Transmit(m message.Message) error {
	if t.bus == nil {
		return fmt.Errorf("Unable to Transmit. Not attached to a bus")
	}
	return t.bus.Transmit(m)
}

// NewSingleTransmitter - Return a new SingleTransmitter
func NewSingleTransmitter() *SingleTransmitter {
	return &SingleTransmitter{}
}

// MultiTransmitter - Tranmits across multiple Buses
type MultiTransmitter struct {
	buses map[Bus]bool
}

// Attach - Attach to given Bus
func (t *MultiTransmitter) Attach(bus Bus) error {
	if t.buses[bus] {
		return fmt.Errorf("Already attached to [%v]", bus)
	}

	t.buses[bus] = true
	return nil
}

// Detach - Detach from the given Bus
func (t *MultiTransmitter) Detach(bus Bus) error {
	if !t.buses[bus] {
		return fmt.Errorf("Not attached to [%v]", bus)
	}

	delete(t.buses, bus)
	return nil
}

// Transmit - Send the Message to all attached Buses
func (t *MultiTransmitter) Transmit(m message.Message) error {
	var err error

	if len(t.buses) == 0 {
		return fmt.Errorf("Not attached to any Bus")
	}

	for bus := range t.buses {
		if err = bus.Transmit(m); err != nil {
			break
		}
	}

	return err
}

// NewMultiTransmitter - Return a new MultiTransmitter
func NewMultiTransmitter() *MultiTransmitter {
	return &MultiTransmitter{}
}
