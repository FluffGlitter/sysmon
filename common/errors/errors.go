package errors

import (
	"bytes"
	"errors"
)

// Errors - Handles multiple errors
type Errors struct {
	buffer bytes.Buffer
}

// Add - Add an error if not nil
func (e *Errors) Add(err error) {
	if err == nil {
		return
	}
	e.buffer.WriteString(err.Error())
}

// Error - Return an error if any were added
func (e *Errors) Error() error {
	if !e.InError() {
		return nil
	}
	return errors.New(e.buffer.String())
}

// InError - Return whether there are any errors
func (e *Errors) InError() bool {
	return e.buffer.Len() > 0
}
