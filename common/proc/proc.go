package proc

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

/*
#include <unistd.h>
*/
import "C"

// UserHz - Return the USER_HZ value
func UserHz() int {
	clkHz := C.sysconf(C._SC_CLK_TCK)
	return int(clkHz)
}

// USER_HZ is 1/100th of a second by default
var userHz = time.Second / time.Duration(UserHz())

// UserHzDuration - Return a time.Duration representing a Duration
// specified in a string using USER_HZ
func UserHzDuration(s string) time.Duration {
	hz, err := strconv.ParseUint(s, 10, 64)
	if err != nil {
		return time.Duration(0)
	}
	return time.Duration(hz) * userHz
}

// Dir - Return the path to a proc filesystem
func Dir() string {
	// TODO: check where a procfs is mounted and return that path
	// or mount one ourself if we cant find one.
	return filepath.FromSlash("/proc")
}

// Path - Return the path to a file in proc filesystem
func Path(file string) string {
	return Dir() + filepath.FromSlash(fmt.Sprintf("/%s", file))
}

// Open - Open a file from proc
func Open(file string) (*os.File, error) {
	return os.Open(Path(file))
}

// PidDir - Return the path to a pids proc dir
func PidDir(pid int) string {
	return Dir() + filepath.FromSlash(fmt.Sprintf("/%d", pid))
}

// PidPath - Return the path to a file in a pids proc dir
func PidPath(pid int, file string) string {
	return PidDir(pid) + filepath.FromSlash(fmt.Sprintf("/%s", file))
}

// PidOpen - Open a file from the pid's proc directory
func PidOpen(pid int, file string) (*os.File, error) {
	return os.Open(PidPath(pid, file))
}
