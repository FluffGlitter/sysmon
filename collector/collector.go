package collector

import (
	"errors"
	"regexp"

	"gitlab.com/FluffGlitter/sysmon/bus"
	"gitlab.com/FluffGlitter/sysmon/bus/message"
	"gitlab.com/FluffGlitter/sysmon/metric"
	"gitlab.com/FluffGlitter/sysmon/options"
)

// Collector - Generic interface for any sort of collector
type Collector interface {
	bus.Attachment

	// GetOpts - Returns the currently set Collector specific options. Should be a structure.
	// Before SetOpts is called, this should return the default options
	// UIs can use this interface to allow end users to set options on a per collector basis
	// (and potentially set all collectors of the same type at the same time)
	GetOpts() (interface{}, error)

	// SetOpts - Sets the Collector specific options.
	// Users should pass in a modified options structure as returned from GetOpts
	SetOpts(interface{}) error

	// Metrics - Return the metrics for this Collector
	// Returns a slice of Metric. This can be called immediately after the Probe function.
	Metrics() []metric.Metric

	message.Transmitter
}

// Prober - Something able to probe for and return Collectors
type Prober interface {
	bus.Prober
	Name() string
}

// The map of registered Collectors
var probers map[string]Prober

// RegisterProber - Register a Prober
// Used by Collector implementations to register thier Probers with the Prober repository.
func RegisterProber(prober Prober) error {
	if probers == nil {
		probers = make(map[string]Prober)
	}

	_, exists := probers[prober.Name()]
	if exists {
		return errors.New("Duplicate Prober Name: " + prober.Name())
	}

	probers[prober.Name()] = prober

	return nil
}

func isBlackListed(p Prober, bl []interface{}) bool {
	for _, v := range bl {
		s, _ := v.(string)
		matched, _ := regexp.MatchString(s, p.Name())
		if matched {
			return true
		}
	}
	return false
}

// ForEachProber - Call the provided function for each Prober
func ForEachProber(callback func(Prober) error, stopOnError bool) {
	l, _ := options.Get("client").Get("collectors.blacklist")
	bl, _ := l.([]interface{})

	for _, p := range probers {
		if isBlackListed(p, bl) {
			continue
		}
		err := callback(p)
		if err != nil && stopOnError {
			break
		}
	}
}

// Probers - Return a slice of the registered Probers
func Probers() []bus.Prober {
	probers := []bus.Prober{}

	ForEachProber(func(p Prober) error {
		probers = append(probers, p)
		return nil
	}, false)

	return probers
}
