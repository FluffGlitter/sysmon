package collector

import (
	"fmt"
	"time"
)

// Sampler - Interface for something that can take a Sampler
type Sampler interface {
	// Sample - Take a Sample
	Sample(time.Time) error
}

// Poller - Interface for a recuring task used for sampling (polling)
type Poller interface {
	// Start - Start the sampling
	Start() error

	// Stop -Stop the sampling
	Stop() error
}

// GenericPoller - Poller to call supplied function regularly
type GenericPoller struct {
	period   time.Duration
	ticker   *time.Ticker
	stopchan chan struct{}
	sampler  Sampler
}

// Start - Start the Poller
func (p *GenericPoller) Start() error {
	if (p.ticker != nil) || (p.stopchan != nil) {
		return fmt.Errorf("Poller already running")
	}
	if p.sampler == nil {
		return fmt.Errorf("Invalid sampler")
	}

	p.stopchan = make(chan struct{})
	p.ticker = time.NewTicker(p.period)

	go func() {
		ticker := p.ticker
		stopchan := p.stopchan
		for {
			select {
			case t := <-ticker.C:
				// TODO: handle any errors. Maybe raise an event once we have designed them?
				p.sampler.Sample(t)
			case <-stopchan:
				return
			}
		}
	}()

	return nil
}

// Stop - Stop the Poller
func (p *GenericPoller) Stop() error {
	p.ticker.Stop()
	p.ticker = nil
	close(p.stopchan)
	p.stopchan = nil
	return nil
}

// NewPoller - Creates a GenereicPoller to call a provided function regularly until stopped
func NewPoller(period time.Duration, sampler Sampler) Poller {
	return &GenericPoller{
		period:  period,
		sampler: sampler,
	}
}

// ConstantPoller - Poller to call supplied Sampler constantly
type ConstantPoller struct {
	stop    chan struct{}
	sampler Sampler
}

// Start - Start sampling
func (p *ConstantPoller) Start() error {
	if p.stop != nil {
		return fmt.Errorf("Poller already running")
	}
	if p.sampler == nil {
		return fmt.Errorf("Invalid sampler")
	}

	p.stop = make(chan struct{})

	go func() {
		stop := p.stop
		for {
			select {
			default:
				p.sampler.Sample(time.Now())
			case <-stop:
				return
			}
		}
	}()

	return nil
}

// Stop - Stop sampling
func (p *ConstantPoller) Stop() error {
	close(p.stop)
	p.stop = nil
	return nil
}

// NewConstantPoller - Create a ConstantPoller
func NewConstantPoller(sampler Sampler) *ConstantPoller {
	return &ConstantPoller{
		sampler: sampler,
	}
}
