package appctx

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"fmt"
	"log"

	"gitlab.com/FluffGlitter/sysmon/options"
	"gitlab.com/FluffGlitter/sysmon/server/db"
)

const (
	keyPath = "server.communication.key"
)

// AppCtx - Context relevant to the application. Stores globals
type AppCtx struct {
	DB  *db.DataStore
	Key *rsa.PrivateKey
}

// New - Return a new AppCtx
func New() *AppCtx {
	provider := options.Get("server").GetString("datastore.provider")
	location := options.Get("server").GetString("datastore.location")

	ctx := &AppCtx{
		DB: db.NewDataStore(provider, location),
	}
	if ctx.DB == nil {
		log.Fatalf("Unable to allocate DataStore[%v:%v]", provider, location)
		return nil
	}

	err := ctx.DB.Open()
	if err != nil {
		log.Fatalf("Unable to open DataStore[%v:%v]: %v", provider, location, err)
	}

	ctx.LoadKey()

	return ctx
}

// Close - Cleanup an AppCtx
func (ctx *AppCtx) Close() {
	err := ctx.DB.Close()
	if err != nil {
		log.Fatalf("Unable to close DataStore[%v:%v]: %v", ctx.DB.Provider, ctx.DB.Path, err)
	}
}

func (ctx *AppCtx) newKey() error {
	reader := rand.Reader
	key, err := rsa.GenerateKey(reader, 512)
	if err != nil {
		return err
	}

	data := x509.MarshalPKCS1PrivateKey(key)
	if data == nil {
		return fmt.Errorf("Unable to create key")
	}

	err = ctx.DB.Set(keyPath, data)
	if err != nil {
		return fmt.Errorf("Unable to store key: %v", err)
	}
	return nil
}

// LoadKey - Load the rsa key from a file
func (ctx *AppCtx) LoadKey() error {
	data := ctx.DB.Get(keyPath)
	if data == nil {
		ctx.newKey()
		data = ctx.DB.Get(keyPath)
	}

	key, err := x509.ParsePKCS1PrivateKey(data)
	if err != nil {
		return fmt.Errorf("Corrupted key: %v", err)
	}
	ctx.Key = key
	return nil
}
