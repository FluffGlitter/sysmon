package pid

import (
	"bufio"
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"gitlab.com/FluffGlitter/sysmon/collector/proc"
)

// State - Process running state
type State byte

// State values
const (
	// Keep Unknown the first item so that default state is Unknown
	Unknown State = iota
	Running
	Sleeping
	Waiting
	Stopped
	TracingStop
	Zombie
	Dead
)

var stateMap = map[string]State{
	"(running)":      Running,
	"(sleeping)":     Sleeping,
	"(disk sleep)":   Waiting,
	"(stopped)":      Stopped,
	"(tracing stop)": TracingStop,
	"(zombie)":       Zombie,
	"(dead)":         Dead,
}

func str2State(s string) State {
	return stateMap[s]
}

type stateDesc struct {
	name string
	desc string
}

var stateStr = map[State]stateDesc{
	Running:     {"Running", "Process is currently running"},
	Sleeping:    {"Sleeping", "Process is sleeping in an interruptible wait"},
	Waiting:     {"Waiting", "Process is waiting on an uninterruptibe disk sleep"},
	Stopped:     {"Stopped", "Process has stopped (on a signal)"},
	TracingStop: {"Tracing", "Process is stopped for tracing"},
	Zombie:      {"Zombie", "Process is zombied!"},
	Dead:        {"Dead", "Process has died"},
}

// String - Return string representation of PID State
func (s State) String() string {
	return stateStr[s].name
}

// Desc - Return a description of the state
func (s State) Desc() string {
	return stateStr[s].desc
}

// UIDSelector type
type UIDSelector uint32

const (
	// Real - The actual UID of the user
	Real UIDSelector = iota
	// Effective - The effective UID of the user (e.g. setuid or sudo)
	Effective
	// Saved - The saved UID when a setuid binary temporarily changes ID
	Saved
	// FS - UID used for filesystem access checks (usually shadows Effective)
	FS

	numUID int = iota
)

// Status - Process status info
type Status struct {
	// The state of the Process
	State State
	// Thread ID of the currently running Thread
	TID int
	// NUMA group ID
	NumaGID uint32
	// Process ID of the parent Process
	ParentPID int
	// Process ID of the Process that is tracing this Process
	TracerPID int
	// User IDs of the user running the process
	UID [numUID]int
	// Group IDs of the user running the process
	GID [numUID]int
	// Number of file descriptor slots currently allocated
	NumFDs uint
	// Supplimentary groups
	Groups []int
	// Peak virtual memory size
	VMPeak uint64
	// Current virtual memory size
	VMCurrent uint64
	// Locked memory size
	VMLocked uint64
	// Pinned memory size
	VMPinned uint64
	// Peak Resident Set Size (high watermark)
	VMPeakRSS uint64
	// Current Resident Set Size
	VMCurrentRSS uint64
	// Data (heap) size
	VMData uint64
	// Stack size
	VMStack uint64
	// Exe text code segment size
	VMText uint64
	// Shared library code size
	VMLib uint64
	// Size of page tables
	VMPTE uint64
	// Size of 2nd level page tables
	VMPMD uint64
	// Swapped out VM size
	VMSwapped uint64
	// Number of threads is process containing this thread
	NumThreads uint
	// TODO: Are signal masks useful for anyone?
	// Mask of CPUs this process can run on
	CPUMask uint
	// Voluntary context switches
	CtxSwitchVoluntary uint64
	// Involuntary context switches
	CtxSwitchInvoluntary uint64
}

func max(a int, b int) int {
	if a >= b {
		return a
	}
	return b
}

func toInts(s []string) []int {
	ret := []int{}
	for i := 0; i < len(s); i++ {
		n, _ := strconv.Atoi(s[i])
		ret = append(ret, n)
	}
	return ret
}

// TODO: Consider using reflection with tags to build
// this map
var statusMap = map[string]func(*Process, []string){
	"State:": func(p *Process, toks []string) {
		p.Status.State = str2State(toks[2])
	},
	"Tgid:": func(p *Process, toks []string) {
		p.PID, _ = strconv.Atoi(toks[1])
	},
	"Ngid:": func(p *Process, toks []string) {
		fmt.Sscanf(toks[1], "%d", &p.Status.NumaGID)
	},
	"Pid:": func(p *Process, toks []string) {
		p.Status.TID, _ = strconv.Atoi(toks[1])
	},
	"PPid:": func(p *Process, toks []string) {
		p.Status.ParentPID, _ = strconv.Atoi(toks[1])
	},
	"TracerPid:": func(p *Process, toks []string) {
		p.Status.TracerPID, _ = strconv.Atoi(toks[1])
	},
	"Uid:": func(p *Process, toks []string) {
		for i := 0; i < max(numUID, len(toks)-1); i++ {
			p.Status.UID[i], _ = strconv.Atoi(toks[i+1])
		}
	},
	"Gid:": func(p *Process, toks []string) {
		for i := 0; i < max(numUID, len(toks)-1); i++ {
			p.Status.GID[i], _ = strconv.Atoi(toks[i+1])
		}
	},
	"FDSize:": func(p *Process, toks []string) {
		fmt.Sscanf(toks[1], "%d", &p.Status.NumFDs)
	},
	"Groups:": func(p *Process, toks []string) {
		p.Status.Groups = append([]int{}, toInts(toks[1:])...)
	},
	"VmPeak:": func(p *Process, toks []string) {
		p.Status.VMPeak, _ = strconv.ParseUint(toks[1], 10, 64)
	},
	"VmSize:": func(p *Process, toks []string) {
		p.Status.VMCurrent, _ = strconv.ParseUint(toks[1], 10, 64)
	},
	"VmLck:": func(p *Process, toks []string) {
		p.Status.VMLocked, _ = strconv.ParseUint(toks[1], 10, 64)
	},
	"VmPin:": func(p *Process, toks []string) {
		p.Status.VMPinned, _ = strconv.ParseUint(toks[1], 10, 64)
	},
	"VmHWM:": func(p *Process, toks []string) {
		p.Status.VMPeakRSS, _ = strconv.ParseUint(toks[1], 10, 64)
	},
	"VmRSS:": func(p *Process, toks []string) {
		p.Status.VMCurrentRSS, _ = strconv.ParseUint(toks[1], 10, 64)
	},
	"VmData:": func(p *Process, toks []string) {
		p.Status.VMData, _ = strconv.ParseUint(toks[1], 10, 64)
	},
	"VmStk:": func(p *Process, toks []string) {
		p.Status.VMStack, _ = strconv.ParseUint(toks[1], 10, 64)
	},
	"VmExe:": func(p *Process, toks []string) {
		p.Status.VMText, _ = strconv.ParseUint(toks[1], 10, 64)
	},
	"VmLib:": func(p *Process, toks []string) {
		p.Status.VMLib, _ = strconv.ParseUint(toks[1], 10, 64)
	},
	"VmPTE:": func(p *Process, toks []string) {
		p.Status.VMPTE, _ = strconv.ParseUint(toks[1], 10, 64)
	},
	"VmPMD:": func(p *Process, toks []string) {
		p.Status.VMPMD, _ = strconv.ParseUint(toks[1], 10, 64)
	},
	"VmSwap:": func(p *Process, toks []string) {
		p.Status.VMSwapped, _ = strconv.ParseUint(toks[1], 10, 64)
	},
	"Threads:": func(p *Process, toks []string) {
		fmt.Sscanf(toks[1], "%d", &p.Status.NumThreads)
	},
	"Cpus_allowed:": func(p *Process, toks []string) {
		m64, _ := strconv.ParseUint(toks[1], 16, 32)
		p.Status.CPUMask = uint(m64)
	},
	"voluntary_ctxt_switches:": func(p *Process, toks []string) {
		p.Status.CtxSwitchVoluntary, _ = strconv.ParseUint(toks[1], 10, 64)
	},
	"nonvoluntary_ctxt_switches:": func(p *Process, toks []string) {
		p.Status.CtxSwitchInvoluntary, _ = strconv.ParseUint(toks[1], 10, 64)
	},
}

// IO - I/O stats
type IO struct {
	// Bytes read from any source
	Read uint64
	// Bytes written to any destination
	Write uint64
	// Read system calls made
	SysRead uint64
	// Write system calls made
	SysWrite uint64
	// Bytes read from disk
	DiskRead uint64
	// Bytes written to disk
	DiskWrite uint64
	// Cancelled writes
	CancelledWrite uint64
}

var ioMap = map[string]func(*Process, []string){
	"rchar:": func(p *Process, toks []string) {
		p.IO.Read, _ = strconv.ParseUint(toks[1], 10, 64)
	},
	"wchar:": func(p *Process, toks []string) {
		p.IO.Write, _ = strconv.ParseUint(toks[1], 10, 64)
	},
	"syscr:": func(p *Process, toks []string) {
		p.IO.SysRead, _ = strconv.ParseUint(toks[1], 10, 64)
	},
	"syscw:": func(p *Process, toks []string) {
		p.IO.SysWrite, _ = strconv.ParseUint(toks[1], 10, 64)
	},
	"read_bytes:": func(p *Process, toks []string) {
		p.IO.DiskRead, _ = strconv.ParseUint(toks[1], 10, 64)
	},
	"write_bytes:": func(p *Process, toks []string) {
		p.IO.DiskWrite, _ = strconv.ParseUint(toks[1], 10, 64)
	},
	"cancelled_write_bytes:": func(p *Process, toks []string) {
		p.IO.CancelledWrite, _ = strconv.ParseUint(toks[1], 10, 64)
	},
}

// TimeLimit - soft and hard time limits
type TimeLimit struct {
	Soft time.Duration
	Hard time.Duration
}

// Limit - soft and hard numerical limits
type Limit struct {
	Soft uint64
	Hard uint64
}

// Limits - Process limits
type Limits struct {
	// Amount of time that the process can run for
	CPUTime TimeLimit
	// File size limits
	FileSize Limit
	// Size of Process' data segment (static data + heap)
	DataSize Limit
	// Size of the Process' Stack
	StackSize Limit
	// Size of the core dump
	CoreSize Limit
	// RSS Size
	RSSSize Limit
	// Number of processes
	Procs Limit
	// Number of open files
	Files Limit
	// Size of allowed locked memory
	MemLock Limit
	// Size of address space
	AddrSpace Limit
	// Number of file locks
	FileLocks Limit
	// Number of pending signals
	SigPend Limit
	// Size of message queue
	MsgQueue Limit
	// Nice priority
	Nice Limit
	// Real time priority
	RTPriority Limit
	// Real time timout
	RTTimeout Limit
}

func limit(s string) uint64 {
	if s == "unlimited" {
		return ^uint64(0)
	}
	v, _ := strconv.ParseUint(s, 10, 64)
	return v
}

var limitMap = map[string]func(*Process, string, string){
	"Max cpu time": func(p *Process, soft, hard string) {
		p.Limits.CPUTime = TimeLimit{
			time.Duration(limit(soft)) * time.Second,
			time.Duration(limit(hard)) * time.Second,
		}
	},
	"Max file size": func(p *Process, soft, hard string) {
		p.Limits.FileSize = Limit{
			limit(soft),
			limit(hard),
		}
	},
	"Max data size": func(p *Process, soft, hard string) {
		p.Limits.DataSize = Limit{
			limit(soft),
			limit(hard),
		}
	},
	"Max stack size": func(p *Process, soft, hard string) {
		p.Limits.StackSize = Limit{
			limit(soft),
			limit(hard),
		}
	},
	"Max core file size": func(p *Process, soft, hard string) {
		p.Limits.CoreSize = Limit{
			limit(soft),
			limit(hard),
		}
	},
	"Max resident set": func(p *Process, soft, hard string) {
		p.Limits.RSSSize = Limit{
			limit(soft),
			limit(hard),
		}
	},
	"Max processes": func(p *Process, soft, hard string) {
		p.Limits.Procs = Limit{
			limit(soft),
			limit(hard),
		}
	},
	"Max open files": func(p *Process, soft, hard string) {
		p.Limits.Files = Limit{
			limit(soft),
			limit(hard),
		}
	},
	"Max locked memory": func(p *Process, soft, hard string) {
		p.Limits.MemLock = Limit{
			limit(soft),
			limit(hard),
		}
	},
	"Max address space": func(p *Process, soft, hard string) {
		p.Limits.AddrSpace = Limit{
			limit(soft),
			limit(hard),
		}
	},
	"Max file locks": func(p *Process, soft, hard string) {
		p.Limits.FileLocks = Limit{
			limit(soft),
			limit(hard),
		}
	},
	"Max pending signals": func(p *Process, soft, hard string) {
		p.Limits.SigPend = Limit{
			limit(soft),
			limit(hard),
		}
	},
	"Max msgqueue size": func(p *Process, soft, hard string) {
		p.Limits.MsgQueue = Limit{
			limit(soft),
			limit(hard),
		}
	},
	"Max nice priority": func(p *Process, soft, hard string) {
		p.Limits.Nice = Limit{
			limit(soft),
			limit(hard),
		}
	},
	"Max realtime priority": func(p *Process, soft, hard string) {
		p.Limits.RTPriority = Limit{
			limit(soft),
			limit(hard),
		}
	},
	"Max realtime timeout": func(p *Process, soft, hard string) {
		p.Limits.RTTimeout = Limit{
			limit(soft),
			limit(hard),
		}
	},
}

// Stat - Info from /proc/<pid>/stat
type Stat struct {
	// Number of minor faults (didnt require loading a page from disk)
	MinFaults uint64
	// Number of major faults (required loading a page from disk)
	MajFaults uint64
	// Number of minor faults caused by waited for children
	ChildMinFaults uint64
	// Number of major faults caused by waited for children
	ChildMajFaults uint64
	// Amount of time spent scheduled in user mode
	UserTime time.Duration
	// Amount of time spent scheduled in kernel mode
	SysTime time.Duration
	// Amount of time waited for children spent in user mode
	ChildUserTime time.Duration
	// Amount of time waited for children spent in kernel mode
	ChildSysTime time.Duration
	// Time since boot when process started
	Started time.Duration
	// Last processor the Process was run on
	Processor int
}

// The key is expected to be the field number of the space
// separated field split line (caller is expected to have handled
// the fact that the process name can include spaces). See
// "man proc" for field numbers
var statMap = map[int]func(*Process, string){
	10: func(p *Process, s string) {
		p.Stat.MinFaults, _ = strconv.ParseUint(s, 10, 64)
	},
	11: func(p *Process, s string) {
		p.Stat.ChildMinFaults, _ = strconv.ParseUint(s, 10, 64)
	},
	12: func(p *Process, s string) {
		p.Stat.MajFaults, _ = strconv.ParseUint(s, 10, 64)
	},
	13: func(p *Process, s string) {
		p.Stat.ChildMajFaults, _ = strconv.ParseUint(s, 10, 64)
	},
	14: func(p *Process, s string) {
		p.Stat.UserTime = proc.UserHzDuration(s)
	},
	15: func(p *Process, s string) {
		p.Stat.SysTime = proc.UserHzDuration(s)
	},
	16: func(p *Process, s string) {
		p.Stat.ChildUserTime = proc.UserHzDuration(s)
	},
	17: func(p *Process, s string) {
		p.Stat.ChildSysTime = proc.UserHzDuration(s)
	},
	22: func(p *Process, s string) {
		p.Stat.Started = proc.UserHzDuration(s)
	},
	39: func(p *Process, s string) {
		p.Stat.Processor, _ = strconv.Atoi(s)
	},
}

// Process - Represents a process
// TODO: Redo this struct w.r.t /proc/<pid>/status instead of stat and statm
type Process struct {
	// Process ID
	PID int
	// Binary name
	Exe string
	// Full command line
	CmdLine string
	// Current working directory
	Cwd string
	// Environment variables (can be empty, only visible to PID and root)
	Env []string
	// Currently opened files
	// TODO: Extend this with access flags
	Files []string
	// I/O stats
	IO IO
	// TODO: limits
	// Root directory of the process
	Root string
	// Status
	Status Status
	// Stat
	Stat Stat
	// Limits
	Limits Limits
}

// handle the status file
func readStatus(pid int, p *Process) error {
	f, err := proc.PidOpen(pid, "status")
	if err != nil {
		return err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		toks := strings.Fields(line)

		f, _ := statusMap[toks[0]]
		if f != nil {
			f(p, toks)
		}
	}

	return nil
}

func readExe(pid int, p *Process) {
	str, _ := filepath.EvalSymlinks(proc.PidPath(pid, "exe"))
	p.Exe = str
}

func scanStrings(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}

	if i := bytes.IndexByte(data, '\x00'); i >= 0 {
		return i + 1, data[0:i], nil
	}

	if atEOF {
		return len(data), data, nil
	}

	return 0, nil, nil
}

func readCmdLine(pid int, p *Process) error {
	f, err := proc.PidOpen(pid, "cmdline")
	if err != nil {
		return err
	}
	defer f.Close()

	buffer := bytes.NewBufferString("")
	scanner := bufio.NewScanner(f)
	scanner.Split(scanStrings)
	scanner.Scan()
	buffer.WriteString(scanner.Text())
	for scanner.Scan() {
		buffer.WriteString(" ")
		buffer.WriteString(scanner.Text())
	}

	p.CmdLine = buffer.String()
	return nil
}

func readCwd(pid int, p *Process) {
	str, _ := filepath.EvalSymlinks(proc.PidPath(pid, "cwd"))
	p.Cwd = str
}

func readRoot(pid int, p *Process) {
	str, _ := filepath.EvalSymlinks(proc.PidPath(pid, "root"))
	p.Root = str
}

func readEnv(pid int, p *Process) error {
	f, err := proc.PidOpen(pid, "environ")
	if err != nil {
		return err
	}
	defer f.Close()

	p.Env = []string{}
	scanner := bufio.NewScanner(f)
	scanner.Split(scanStrings)
	for scanner.Scan() {
		p.Env = append(p.Env, scanner.Text())
	}

	return nil
}

func readFiles(pid int, p *Process) error {
	files, err := ioutil.ReadDir(proc.PidPath(pid, "fd"))
	if err != nil {
		return err
	}

	for _, f := range files {
		//str, err := filepath.EvalSymlinks(proc.PidPath(pid, fmt.Sprintf("fd/%s", f.Name())))
		str, err := os.Readlink(proc.PidPath(pid, fmt.Sprintf("fd/%s", f.Name())))
		if err != nil {
			log.Printf("readFiles: %v", err)
			continue
		}
		p.Files = append(p.Files, str)
	}
	return nil
}

func readIO(pid int, p *Process) error {
	f, err := proc.PidOpen(pid, "io")
	if err != nil {
		return err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		toks := strings.Fields(line)

		f, _ := ioMap[toks[0]]
		if f != nil {
			f(p, toks)
		}
	}

	return nil
}

func readLimits(pid int, p *Process) error {
	f, err := proc.PidOpen(pid, "limits")
	if err != nil {
		return err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		name := strings.TrimSpace(line[:25])
		soft := strings.TrimSpace(line[26:45])
		hard := strings.TrimSpace(line[46:65])

		f, _ := limitMap[name]
		if f != nil {
			f(p, soft, hard)
		}
	}
	return nil
}

func readStat(pid int, p *Process) error {
	f, err := proc.PidOpen(pid, "stat")
	if err != nil {
		return err
	}
	defer f.Close()

	b, err := ioutil.ReadAll(f)
	if err != nil {
		return err
	}

	line := string(b)
	line = line[strings.LastIndex(line, ")")+1:]
	toks := strings.Fields(line)

	for i, t := range toks {
		f, _ := statMap[i+3]
		if f != nil {
			f(p, t)
		}
	}
	return nil
}

// Get - Return (or fill) a Process for a given pid
func Get(pid int, proc *Process) (*Process, error) {
	if proc == nil {
		proc = &Process{}
	}

	readStatus(pid, proc)
	readExe(pid, proc)
	readCmdLine(pid, proc)
	readCwd(pid, proc)
	readRoot(pid, proc)
	readEnv(pid, proc)
	readFiles(pid, proc)
	readIO(pid, proc)
	readLimits(pid, proc)
	readStat(pid, proc)

	return proc, nil
}
