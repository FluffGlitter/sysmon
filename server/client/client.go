package client

import (
	"bytes"
	"encoding/gob"
	"net"

	"github.com/satori/go.uuid"
	"gitlab.com/FluffGlitter/sysmon/server/api"
	"gitlab.com/FluffGlitter/sysmon/server/appctx"
)

type clientData interface {
	Version() int
	Convert(name string) (*Client, bool)
}

// clientData - Data to be stored for the client
type clientDataV1 struct {
	Key         []byte
	IP          net.IP
	Authorized  bool
	Provisioned bool
}

func (d *clientDataV1) Version() int {
	return 1
}

func (d *clientDataV1) Convert(name string) (*Client, bool) {
	c := &Client{
		Name:         name,
		clientDataV1: *d,
	}
	return c, false
}

// Client - Represents a client that has been registerd with the server
type Client struct {
	Name string
	UUID uuid.UUID
	clientDataV1
	seq uint64
}

// new - Return a new client created from the supplied Register data
func new(info *api.Register, ip net.IP) *Client {
	client := &Client{
		Name: info.Name,
		UUID: info.UUID,
	}
	copy(client.Key, info.Key)
	copy(client.IP, ip)

	return client
}

func update(client *Client, info *api.Register, ip net.IP) *Client {
	c := Client{}
	c = *client
	c.Name = info.Name
	c.UUID = info.UUID
	copy(c.Key, info.Key)
	copy(c.IP, ip)

	if needsNew(client, &c) {
		c.Provisioned = false
		c.Authorized = false
	}
	return &c
}

// Register - Register (or update) a Client
func Register(ctx *appctx.AppCtx, info *api.Register, ip net.IP) (*Client, error) {
	c := Get(ctx, info.Name, info.UUID)
	if c == nil {
		c = new(info, ip)
		Put(ctx, c)
		return c, nil
	}

	c = update(c, info, ip)
	err := Put(ctx, c)
	if err != nil {
		return nil, err
	}
	return c, nil
}

// Get - Get a Client by name
func Get(ctx *appctx.AppCtx, name string, uuid uuid.UUID) *Client {
	var data clientData
	seq, b := ctx.DB.GetLast("clients." + name + "." + uuid.String())
	if b == nil {
		return nil
	}
	d := gob.NewDecoder(bytes.NewReader(b))
	err := d.Decode(&data)
	if err != nil {
		return nil
	}

	c, converted := data.Convert(name)
	c.seq = seq
	if converted {
		Put(ctx, c)
	}

	return c
}

func needsNew(prev, new *Client) bool {
	if prev == nil {
		return true
	}
	if bytes.Compare(prev.Key, new.Key) != 0 {
		return true
	}
	if prev.Name != new.Name {
		// this should never happen, but just in case...
		return true
	}
	if !prev.IP.Equal(new.IP) {
		return true
	}
	return false
}

// Put - Store a Client
func Put(ctx *appctx.AppCtx, client *Client) error {
	prev := Get(ctx, client.Name, client.UUID)

	b := bytes.NewBuffer(nil)
	e := gob.NewEncoder(b)
	err := e.Encode(client)
	if err != nil {
		return err
	}

	if needsNew(prev, client) {
		err = ctx.DB.SetNext("client"+client.Name+"."+client.UUID.String(), b.Bytes())
	} else {
		err = ctx.DB.SetSeq("client."+client.Name+"."+client.UUID.String(), client.seq, b.Bytes())
	}
	if err != nil {
		return err
	}
	return nil
}

func init() {
	gob.Register(&clientDataV1{})
}
