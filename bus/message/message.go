package message

import (
	"fmt"
	"time"
)

// Message - Abstract item to be communicated via a bus
type Message interface {
	Timestamp() time.Time
	Data() interface{}
}

// Receiver - Abstract interface for anything that can receive a Message
type Receiver interface {
	Receive(m Message) error
}

// Transmitter - Abstract interface for anything that can transmit a Message
type Transmitter interface {
	Transmit(m Message) error
}

// Broadcaster - Recevie a mesage and duplicate it to multiple Receivers
type Broadcaster struct {
	receivers map[Receiver]bool
}

// Receive - Receive a Message and duplicate
func (b *Broadcaster) Receive(m Message) error {
	var err error

	for r := range b.receivers {
		if err = r.Receive(m); err != nil {
			break
		}
	}

	return err
}

// AddReceiver - Add a Receiver
func (b *Broadcaster) AddReceiver(r Receiver) error {
	if b.receivers[r] {
		return fmt.Errorf("Receiver [%v] already Added", r)
	}

	b.receivers[r] = true
	return nil
}

// RemoveReceiver - Remove a Receiver
func (b *Broadcaster) RemoveReceiver(r Receiver) error {
	if !b.receivers[r] {
		return fmt.Errorf("Receiver [%v] not present", r)
	}

	delete(b.receivers, r)
	return nil
}

// NewBroadcaster - Return a new Broadcaster
func NewBroadcaster() *Broadcaster {
	return &Broadcaster{receivers: make(map[Receiver]bool)}
}
