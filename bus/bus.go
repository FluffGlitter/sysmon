package bus

import (
	"fmt"
	"sync"

	"gitlab.com/FluffGlitter/sysmon/bus/message"
	"gitlab.com/FluffGlitter/sysmon/common/errors"
)

// StartStopper - Something that can be started and stopped
type StartStopper interface {
	Start() error
	Stop() error
}

// Descriptor - Describes something attached to the bus
type Descriptor interface {
	Name() string
	Instance() uint32
}

// Attachment - Something that can be attached and detached to a bus
type Attachment interface {
	Attach(bus Bus) error
	Detach(bus Bus) error
	Descriptor
	StartStopper
}

func key(a Attachment) string {
	return fmt.Sprintf("%s.%d", a.Name(), a.Instance())
}

// Prober - Something that can be probed
type Prober interface {
	Probe(bus Bus) ([]Attachment, error)
}

// Bus - Communication channel for Samples
type Bus interface {
	Attach(a Attachment) error
	Detach(a Attachment) error
	Probe([]Prober) error
	message.Transmitter
	message.Receiver
	StartStopper
}

// ChannelBus - Bus over a channel
type ChannelBus struct {
	channel     chan message.Message
	stop        chan struct{}
	once        *sync.Once
	wg          sync.WaitGroup
	attachments map[string]Attachment
	*message.Broadcaster
}

// Attach - Attach the Attachement to the bus
func (c *ChannelBus) Attach(a Attachment) error {
	name := key(a)
	_, exists := c.attachments[name]
	if exists {
		return fmt.Errorf("Already attached [%v]<-[%v]", c, a)
	}
	c.attachments[name] = a
	a.Attach(c)

	return nil
}

// Detach - Detach the Attachment from the bus
func (c *ChannelBus) Detach(a Attachment) error {
	name := key(a)
	_, exists := c.attachments[name]
	if !exists {
		return fmt.Errorf("Not currently attached [%v]<-[%v]", c, a)
	}
	a.Detach(c)
	delete(c.attachments, name)

	return nil
}

// Probe - Call the given Probers and attach their attachments
func (c *ChannelBus) Probe(probers []Prober) error {
	var errors errors.Errors

	for _, p := range probers {
		attachments, err := p.Probe(c)
		if err != nil {
			errors.Add(err)
			continue
		}
		for _, a := range attachments {
			err := c.Attach(a)
			if err != nil {
				errors.Add(err)
				continue
			}
		}
	}

	return nil
}

func (c *ChannelBus) startListening() {
	c.once.Do(func() {
		c.wg.Add(1)

		go func() {
			defer c.wg.Done()

			for {
				select {
				case <-c.stop:
					return

				case msg := <-c.channel:
					c.Receive(msg)
				}
			}
		}()
	})
}

// Transmit - Transmit a message through the bus
func (c *ChannelBus) Transmit(m message.Message) error {
	c.startListening()
	c.channel <- m
	return nil
}

// Start - Start all Attachments attached to the bus
func (c *ChannelBus) Start() error {
	var errors errors.Errors

	c.startListening()

	for _, a := range c.attachments {
		errors.Add(a.Start())
	}

	return errors.Error()
}

// Stop - Stop all Attachments attached to the bus
func (c *ChannelBus) Stop() error {
	var errors errors.Errors

	for _, a := range c.attachments {
		errors.Add(a.Stop())
	}

	if errors.InError() {
		return errors.Error()
	}

	// All Attachments have been stopped. We can stop the service routine
	c.stop <- struct{}{}
	c.wg.Wait()
	c.once = &sync.Once{}

	return nil
}

// NewChannelBus - Create a new ChannelBus
func NewChannelBus() *ChannelBus {
	return &ChannelBus{
		// TODO: get channel buffer size from options
		channel:     make(chan message.Message, 10),
		stop:        make(chan struct{}),
		once:        &sync.Once{},
		attachments: make(map[string]Attachment),
		Broadcaster: message.NewBroadcaster(),
	}
}
