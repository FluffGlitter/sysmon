package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"sync"

	"net/http"
	_ "net/http/pprof"

	"gitlab.com/FluffGlitter/sysmon/bus"
	"gitlab.com/FluffGlitter/sysmon/bus/message"
	"gitlab.com/FluffGlitter/sysmon/client/db"
	"gitlab.com/FluffGlitter/sysmon/collector"
	_ "gitlab.com/FluffGlitter/sysmon/collector/collectors"
	"gitlab.com/FluffGlitter/sysmon/options"
	"gitlab.com/FluffGlitter/sysmon/sample"
)

type loggingReceiver struct {
}

// Receive - Receive a message and log it
func (l *loggingReceiver) Receive(msg message.Message) error {
	switch msg.(type) {
	case sample.Sample:
		s := msg.(sample.Sample)
		log.Printf("%v Sample: From[%v.%v] {%v}", s.Timestamp(), s.Metric().Name(), s.Metric().Instance(), s.Data())
	default:
		log.Printf("%v Unknown msg type [%v]: {%v}", msg.Timestamp(), msg, msg.Data())
	}

	return nil
}

type localStorageReceiver struct {
	*db.LocalDB
}

// Receive - Receive a mesage and store it in the database
func (d *localStorageReceiver) Receive(msg message.Message) error {
	go func() {
		switch msg.(type) {
		case sample.Sample:
			s := msg.(sample.Sample)
			d.StoreSample(s)
		default:
		}
	}()

	return nil
}

func newLocalStorageReceiver() *localStorageReceiver {
	var err error
	ret := &localStorageReceiver{}
	ret.LocalDB, err = db.NewLocalDB()
	if err != nil {
		return nil
	}
	ret.Open()

	return ret
}

func waitForExit() {
	wg := sync.WaitGroup{}
	wg.Add(1)

	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	go func() {
		<-ch
		wg.Done()
	}()

	wg.Wait()
}

var profiling = flag.String("profiling", "", "Enable profiling")

func main() {
	var opts *options.Options
	opts = options.New("client", nil)
	err := opts.Load(filepath.FromSlash("config/client.conf"))
	if err != nil {
		opts = options.New("client", []byte(`{"collectors": {"blacklist": ["^Dummy\\..*$"]}}`))
	}
	options.Register(opts)

	bus := bus.NewChannelBus()
	bus.Probe(collector.Probers())
	bus.AddReceiver(&loggingReceiver{})
	bus.AddReceiver(newLocalStorageReceiver())
	bus.Start()
	defer bus.Stop()

	go func() {
		log.Println(http.ListenAndServe(":6060", nil))
	}()
	waitForExit()
}
