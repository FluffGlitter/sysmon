package db

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/boltdb/bolt"
)

// DataStore - Represents a data store for generic info
type DataStore struct {
	Provider string
	Path     string
	db       *bolt.DB
}

// NewDataStore - Allocate a new DataStore ready to Open
func NewDataStore(provider, path string) *DataStore {
	// Currently we only support boltdb
	if provider != "boltdb" {
		return nil
	}
	return &DataStore{
		Provider: provider,
		Path:     path,
	}
}

// Open - Open the DataStore ready for transactions
func (d *DataStore) Open() error {
	var err error
	d.db, err = bolt.Open(d.Path, 0600, nil)
	if err != nil {
		return err
	}
	return nil
}

// Close - Close the DataStore
func (d *DataStore) Close() error {
	return d.db.Close()
}

func stringsPath(strs []string) string {
	if strs == nil || len(strs) == 0 {
		return ""
	}
	ret := strs[0]
	for i := 1; i < len(strs); i++ {
		ret += "." + strs[i]
	}
	return ret
}

func getBucket(tx *bolt.Tx, path string) (*bolt.Bucket, []string, string) {
	toks := strings.Split(path, ".")

	// we mandate that all keys must be in a bucket. No top level keys here
	if len(toks) == 1 {
		return nil, nil, path
	}

	b := tx.Bucket([]byte(toks[0]))
	if b == nil {
		return nil, toks[:len(toks)-1], toks[len(toks)-1]
	}
	for i := 1; i < len(toks)-1; i++ {
		n := b.Bucket([]byte(toks[i]))
		if n == nil {
			return b, toks[i : len(toks)-1], toks[len(toks)-1]
		}
		b = n
	}

	return b, nil, ""
}

func seqKey(seq uint64) []byte {
	return []byte(strconv.FormatUint(seq, 10))
}

// Get - Get a value from DataStore
// path is a '.' separated path to key in DataStore
func (d *DataStore) Get(path string) []byte {
	var ret []byte
	err := d.db.View(func(tx *bolt.Tx) error {
		b, r, k := getBucket(tx, path)
		if b == nil {
			return fmt.Errorf("No such bucket: %v", r)
		}
		if r != nil {
			return fmt.Errorf("Missing buckets: %v", stringsPath(r))
		}
		if k == "" {
			// The path pointed to a bucket, should have pointed to a key
			return fmt.Errorf("path points to bucket")
		}
		v := b.Get([]byte(k))
		if v == nil {
			return fmt.Errorf("No such key: %v", path)
		}
		copy(ret, v)
		return nil
	})
	if err != nil {
		return nil
	}

	return ret
}

// GetLast - Get the last sequenced entry from bucket in DataStore
// path is a '.' separated path to a bucket in DataStore
func (d *DataStore) GetLast(path string) (uint64, []byte) {
	var retval []byte
	var retkey uint64
	err := d.db.View(func(tx *bolt.Tx) error {
		b, r, k := getBucket(tx, path)
		if b == nil {
			return fmt.Errorf("No such bucket: %v", path)
		}
		if r != nil {
			return fmt.Errorf("Missing buckets: %v", stringsPath(r))
		}
		if k != "" {
			return fmt.Errorf("path does not resolve to bucket: %v", path)
		}

		seq := b.Sequence()
		v := b.Get(seqKey(seq))
		if v == nil {
			return fmt.Errorf("Invalid Seq, no such key: %v.%v", path, string(seq))
		}
		copy(retval, v)
		retkey = seq
		return nil
	})
	if err != nil {
		return 0, nil
	}
	return retkey, retval
}

// ForEach - Call provided func for each key/value pair in the bucket
// path is a '.' separated path to a bucket in DataStore
// key and val as passed to the func should be considered constant and only available
// for the lifetime of the call. No other Set/Get calls should be made from within the
// function. If f returns an error, no further calls will be made and the call will
// return with the error
func (d *DataStore) ForEach(path string, f func(key, val []byte) error) error {
	err := d.db.View(func(tx *bolt.Tx) error {
		b, r, k := getBucket(tx, path)
		if b == nil {
			return fmt.Errorf("No such bucket: %v", path)
		}
		if r != nil {
			return fmt.Errorf("Missing buckets: %v", stringsPath(r))
		}
		if k != "" {
			return fmt.Errorf("path does not resolve to bucket: %v", path)
		}

		return b.ForEach(f)
	})
	return err
}

func makeBuckets(tx *bolt.Tx, root *bolt.Bucket, buckets []string) (*bolt.Bucket, error) {
	if root == nil {
		b, err := tx.CreateBucket([]byte(buckets[0]))
		if err != nil {
			return nil, err
		}
		if len(buckets) == 1 {
			return b, nil
		}
		buckets = buckets[1:]
		root = b
	}

	for i := 0; i < len(buckets); i++ {
		b, err := root.CreateBucket([]byte(buckets[i]))
		if err != nil {
			return root, err
		}
		root = b
	}

	return root, nil
}

// Set - Set a value in the DataStore
// path is a '.' separated path to the key in DataStore
func (d *DataStore) Set(path string, val []byte) error {
	err := d.db.Update(func(tx *bolt.Tx) error {
		var err error
		b, r, k := getBucket(tx, path)
		if b == nil && r == nil {
			return fmt.Errorf("path should include bucket: %v", path)
		}
		if k == "" {
			return fmt.Errorf("path points to bucket")
		}
		if r != nil {
			b, err = makeBuckets(tx, b, r)
			if err != nil {
				return err
			}
		}
		return b.Put([]byte(k), val)
	})
	return err
}

// SetNext - Set a value in the DataStore
// The path will be the next sequence number for the bucket
// path is a '.' separated path to the bucket in DataStore
func (d *DataStore) SetNext(path string, val []byte) error {
	err := d.db.Update(func(tx *bolt.Tx) error {
		var err error
		b, r, k := getBucket(tx, path)
		if b == nil && r == nil {
			return fmt.Errorf("path should point to a bucket: %v", path)
		}
		if k != "" {
			return fmt.Errorf("path does not resolve to bucket: %v", path)
		}
		if r != nil {
			b, err = makeBuckets(tx, b, r)
			if err != nil {
				return err
			}
		}
		seq, err := b.NextSequence()
		if err != nil {
			return err
		}
		return b.Put([]byte(strconv.FormatUint(seq, 10)), val)
	})
	return err
}

// SetSeq - Overwrite the sequence tracked entry in the bucket in the DataStore
func (d *DataStore) SetSeq(path string, seq uint64, val []byte) error {
	err := d.db.Update(func(tx *bolt.Tx) error {
		var err error
		b, r, k := getBucket(tx, path)
		if b == nil && r == nil {
			return fmt.Errorf("path should point to a bucket: %v", path)
		}
		if k != "" {
			return fmt.Errorf("path does not resolve to bucket: %v", path)
		}
		if r != nil {
			b, err = makeBuckets(tx, b, r)
			if err != nil {
				return err
			}
		}
		s := b.Sequence()
		if s < seq {
			return fmt.Errorf("seq > bucket sequence: %v > %v", seq, s)
		}
		return b.Put([]byte(strconv.FormatUint(seq, 10)), val)
	})
	return err
}

// GetSequence - Get a sequence number for the bucket in the DataStore
// path is a '.' separated path the the bucket in DataStore
func (d *DataStore) GetSequence(path string) (uint64, error) {
	var ret uint64
	err := d.db.View(func(tx *bolt.Tx) error {
		var err error
		b, r, k := getBucket(tx, path)
		if b == nil && r == nil {
			return fmt.Errorf("path should point to a bucket: %v", path)
		}
		if k != "" {
			return fmt.Errorf("path does not resolve to bucket: %v", path)
		}
		if r != nil {
			b, err = makeBuckets(tx, b, r)
			if err != nil {
				return err
			}
		}
		ret = b.Sequence()
		return nil
	})
	if err != nil {
		return 0, err
	}
	return ret, nil
}
